#include <xmmintrin.h>
#include <emmintrin.h>
#include <pmmintrin.h>

#include <stdio.h>

#define N 8



int main(int argc,char *argv[]){
	float a[N] __attribute__((aligned(16)));
	
	
	
	

	for (size_t i = 0;i<N;i++){
		a[i] = i +1;
	}
	int j = 0;
	__m128 v2 =  _mm_set_ps(0,0,0,1);
	__m128 v22 =  _mm_set_ps(1,1,1,0);
	__m128 v3 =  _mm_set_ps(0,0,1,1);
	__m128 v33 =  _mm_set_ps(1,1,0,0);
	__m128 v4 =  _mm_set_ps(0,1,1,1);
	__m128 v44 =  _mm_set_ps(1,0,0,0);
	__m128 acc2;
		__m128 acc3;
__m128 acc;


	for (size_t i=0;i<N;i+=4){

		__m128 v = _mm_load_ps(&a[i]);
		acc = _mm_load_ps(&a[i]);
		

		for (int k=0;k<a[j];k++){ // for para elever hasta exponente igual al menor
			acc = _mm_mul_ps(acc,v); // vector sera de la forma X/X/X/X
		}

		acc2 = _mm_mul_ps(v,v4);	//vector sera de la forma 0/X/x/x
		for (int k=a[j];k<a[j+1];k++){
			acc3 = _mm_mul_ps(acc3,acc2); 
		}
		acc3 = _mm_add_ps(acc3,v44); // vector sera de la forma 1/X/X/X y puedo multiplicar con forma X/X/X/X
		acc = _mm_mul_ps(acc,acc3);

		acc2 = _mm_mul_ps(v,v3);	//vector sera de la forma 0/0/x/x
		for (int k=a[j+1];k<a[j+2];k++){
			acc3 = _mm_mul_ps(acc3,acc2); 
		}
		acc3 = _mm_add_ps(acc3,v33); // vector sera de la forma 1/1/X/X y puedo multiplicar con forma X/X/X/X
		acc = _mm_mul_ps(acc,acc3);

		acc2 = _mm_mul_ps(v,v2);	//vector sera de la forma 0/0/x/x
		for (int k=a[j+2];k<a[j+3];k++){
			acc3 = _mm_mul_ps(acc3,acc2); 
		}
		acc3 = _mm_add_ps(acc3,v22); // vector sera de la forma 1/1/X/X y puedo multiplicar con forma X/X/X/X
		acc = _mm_mul_ps(acc,acc3);

				
		acc = _mm_sqrt_ps(acc);  //Saco la raiz de del vector b
		
		
		j = j + 4;
		
	}

	_mm_store_ps(a,acc);
	float result=a[0]+a[1]+a[2]+a[3];
	
	printf("%f\n",result);
}
	