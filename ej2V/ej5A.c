#include <xmmintrin.h>
#include <emmintrin.h>
#include <pmmintrin.h>
#include <stdio.h>
#include <malloc.h>
#include <math.h>

#include <stdio.h>

#define N 4



int main(int argc,char *argv[]){
	float a[N] __attribute__((aligned(16)));
	float b[N] __attribute__((aligned(16)));
	float c[N] __attribute__((aligned(16)));
	float restriccion;


	
	
	
	

	for (size_t i = 0;i<N;i++){ // valores normales del vector
		a[i] = i +1;
		b[i] = i + 2; // valores i + 1 del vector
	}
	
	__m128 v2;
	for (size_t i=0;i<N;i+=4){ // ocupo calculo vectorial para obtener el |V|

		__m128 v = _mm_load_ps(&a[i]);
		v2 = _mm_mul_ps(v2,v2);
	}

	_mm_store_ps(c,v2); // obtuve la suma de los cuadrados
	restriccion = c[0]+c[1]+c[2]+c[3];
	restriccion = sqrt(restriccion);  //le aplico la raiz a la suma de cuadrados para obtener restriccion final

	for (size_t i = 0;i<N;i++){  // a cada elemento que no cumple la restriccion lo hago 0
		if (a[i]<restriccion){	// por lo cual no afectara al momento de hacer sumatoria
			a[i] = 0;
		}

		
		if (b[i]<restriccion){
			b[i] = 0;
		}
						
	}




		__m128 acc;
for (size_t i=0;i<N;i+=4){

		__m128 v3 = _mm_load_ps(&a[i]); 
		__m128 v4 = _mm_load_ps(&b[i]);	//cargo el vectorps
		v3 = _mm_mul_ps(v3,v4);		//cargo el vector ¿3 veces?
		acc = _mm_add_ps(acc,v3);

		
	}
 _mm_store_ps(a,acc);
	printf("%f\n",a[0]+a[1]+a[2]+a[3]);
}