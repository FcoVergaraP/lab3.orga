#include <stdio.h>
#include <malloc.h>
#include <math.h>

#define N 4

float calcular(float *a);

int main(int argc,char *argv[]){
	float a[N];
	float b[N];

	for (size_t i = 0;i<N;i++){
		a[i] = i +1;
	}
	

	printf("%f\n", calcular(a));

	return 0;

}

float calcular(float *a){
	float acc = 0;
	float acc2 = 0;

	for (size_t i=0;i<N;i++){
		acc = pow(a[i],2);
		acc2 = acc2 + acc; // sumatoria de cuadrados

	}

	float restriccion = sqrt(acc2);	// obtengo la restriccion de sumatoria
	acc =0;
	for (size_t i=0;i<N-1;i++){
		if (i<restriccion){
			acc2 = a[i]*a[i+1];
			acc = acc + acc2;
		}

	}
	
	

	return acc;

	}	


