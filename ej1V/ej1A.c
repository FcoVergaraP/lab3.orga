#include <xmmintrin.h>
#include <emmintrin.h>
#include <pmmintrin.h>

#include <stdio.h>

#define N 800



int main(int argc,char *argv[]){
	float a[N] __attribute__((aligned(16)));
	float b[N] __attribute__((aligned(16)));
	
	
	

	for (size_t i = 0;i<N;i++){
		a[i] = i +1;
	}
	for (size_t i = 0; i<N;i++){
		b[i]=i+1;
	}
	

		 
	__m128 acc;
	__m128 v4;
	__m128 v3;
	__m128 v2;
	__m128 v5;
	__m128 acc2;
	__m128 v6;

	float result;
	
	for (size_t i=0;i<N;i+=4){
		__m128 v = _mm_load_ps(&a[i]);
		 v2 = _mm_load_ps(&b[i]);
		float Num2 = 1;
		 v5 = _mm_set1_ps(Num2);
		 v5 = _mm_add_ps(v5,v5); //Obtengo el numero 2 en todos los ordenes del vector.


		v = _mm_div_ps(v,v5);  //Divido el vector por 2
		v2 = _mm_sqrt_ps(v2);  //Saco la raiz de del vector b
		acc = _mm_add_ps(acc,_mm_add_ps(v,v2)); // sumo el anterior mas LA RAIZ MAS VALOR DIVIDO 2
		
		
	}
	
	_mm_store_ps(b,acc);
	result=b[0]+b[1]+b[2]+b[3];
	
	printf("%f\n",result);
	

	for (size_t i=0;i<N;i+=4){

		__m128 v = _mm_load_ps(&a[i]); 
		__m128 vr = _mm_set1_ps(result);	//cargo el vectorps
		v6 = _mm_load_ps(&a[i]);		//cargo el vector ¿3 veces?
		v6 = _mm_mul_ps(v,v);
		v = _mm_sub_ps(v6,vr);
		acc2 = _mm_add_ps(acc2,v);			// saco cuadrado

		
	}
 _mm_store_ps(a,acc2);
	printf("%f\n",a[0]+a[1]+a[2]+a[3]);
	
	return 0;
}