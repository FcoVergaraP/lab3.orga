#include <xmmintrin.h>
#include <emmintrin.h>
#include <pmmintrin.h>

#include <stdio.h>

#define N 1000*1000




int main(int argc,char *argv[]){
	float a[N] __attribute__((aligned(16)));
	float b[N] __attribute__((aligned(16)));
	

	for (size_t i = 0;i<N;i++){
		a[i] = i +1;
	}
	for (size_t i = 0; i<N;i++){
		b[i]=i+1;
	}
	

		 
	__m128 v3;
	__m128 v4;
	
	__m128 v2;
	__m128 v5;
	__m128 v6;

	float result;
	
	for (int i=0;i<N;i+=8){
		__m128 v = _mm_load_ps(&a[i]);
		 v2 = _mm_load_ps(&b[i]);
		 v5 = v/2;

		v = _mm_div_ps(v,v5);
		v2 = _mm_sqrt_ps(v2);
		v = _mm_add_ps(v,v2);
		v3 = _mm_add_ps(v3,v);
		
	}
	for (int i=0;i<N;i+=8){

		__m128 v = _mm_load_ps(&a[i]);
		v6 = _mm_load_ps(&a[i]);
		v = _mm_mul_ps(v,v6);
		v4 = _mm_add_ps(v,v4);
		v4 = _mm_sub_ps(v4,v3);
		
	}
 _mm_store_ps(a,v4);
	printf("%f\n",a[0]+a[1]+a[2]+a[3]);

	return 0;
}