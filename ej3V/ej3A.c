#include <xmmintrin.h>
#include <emmintrin.h>
#include <pmmintrin.h>

#include <stdio.h>

#define N 800



int main(int argc, char *argv[])
{
    float a[N] __attribute__((aligned(16)));
    float b[N] __attribute__((aligned(16)));

    // NO hago entrada ahora, solamente números sin significado
    for(size_t i = 0; i < N-1; i++){
        a[i] = i % 120 + 1;
    }

     for(size_t i = 1; i < N; i++){
        b[i] = i % 120 + 1;
    }

    __m128 acc;

    for (size_t i=0;i<N;i+=4){

    

    __m128 v = _mm_load_ps(&a[i]);
    __m128 v2 = _mm_load_ps(&b[i]);
    __m128 v3 = _mm_xor_ps(v,v2);
    acc = _mm_add_ps(acc,v3);    

    }

    _mm_store_ps(a,acc);
    printf("%f\n",a[0]+a[1]+a[2]+a[3]);
    /*


    int acc = 0;

    for(size_t i = 0; i < N - 1; i++)
    {
       char tmp = a[i] ^ a[i + 1];
       acc = acc + tmp;
    }*/

    return 0;
}